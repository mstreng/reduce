load_attach_mode(True, True)
load("load_table.sage")
load("mestre.sage")
load("reduction.sage")
load("stoll_cremona.sage")
load("ideals.sage")
import os
cm_curve_table_base_url = "file://" + os.path.abspath("data") + "/"