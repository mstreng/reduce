These data files belong to Bouyer-Streng -- Examples of genus two curves with
CM defined over the reflex field, arXiv:1307.0486.

For 1a, each line is:
[D,A,B], [[i1], [i2], [i3]],[[a0], [a1], [a2], [a3], [a4], [a5], [a6]]
where the final ", [a6]" is omitted if it is zero.
Such a line corresponds to the curve y^2 = sum ai x^i over QQ, where the
absolute Igusa invariants i1, i2, and i3 are the choice of invariants
that is advertised in [S12].
The field K is QQ[X]/(X^4+A*X^2+B) and the discriminant of K_0 is D.

For 1b and 2b, each line is:
[D,A,B],[n, e, 1],[i1, i2, i3],[a0, a1, a2, a3, a4, a5, a6],
where ik and ak are lists of length two.
Let a^2 + e*a + n = 0, then a list [x,y] for ik or ak represents the number
x+a*y in ZZ[a] = O_{K_0^r}.
Then D,A,B, ik, and ak have the same meaning as for table 1a.

For 2c, the ak are omitted as there is no model over K_0^r. Instead, the local
obstructions are given in some data format that is not (yet) specified in this
readme.


[S12] - Streng -- Computing Igusa class polynomials, arXiv:0903.4766
