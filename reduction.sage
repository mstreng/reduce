r"""
Reduction of binary forms and hyperelliptic curve equations

This file contains functions that reduce the coefficients of a binary form or
hyperelliptic curve

AUTHORS:

- Florian Bouyer (2011 - 09) : initial version
- Marco Streng (2012 - 02) : some editing and rearrangement

"""
#*****************************************************************************
#       Copyright (C) 2011 Florian Bouyer <f.j.s.c.bouyer@gmail.com>
#
#  Distributed under the terms of the GNU General Public License (GPL)
#  as published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from sage.structure.sage_object import SageObject
from sage.matrix.all import Matrix
from sage.schemes.plane_conics.constructor import Conic
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.arith.misc import gcd, primes_first_n, factor
from sage.rings.all import ZZ, QQ, CC, RR, ComplexField, RealField, FiniteField
from sage.schemes.hyperelliptic_curves.constructor import HyperellipticCurve

#from sage.schemes.hyperelliptic_curves.stoll_cremona import stoll_cremona_reduction


def reduce_polynomial(f, degree=None, precision=3000, algorithm='default',
                          factors=[], transformation = False):
    r"""
    Reduces the height of f by using the functions reduce_discriminant and
    stoll_cremona_reduction.
    (See their respective documentations for details)
    
    INPUT:
        
    - ``f`` - a polynomial
    
    - ``degree`` - If ``degree`` is n, interpret f as the binary form f(X/Z)*Z^n
    of degree n.
    
    - ``precision`` - an integer (default: 3000) the bit precision of the
    numbers in `\CC` and `\RR` that is going to be used.

    - ``algorithm`` - default/magma. If set to magma in the stoll_cremona
    reduction step, the function uses magma to calculate a 'better' covariant
    of f

    - ``factors`` (default:[]) -- a list of ideals. If supplied, then use these
    to help factor the discriminant.
    
    - ``transformation`` (default = False) - boolean. If true, then as well as
    returning the reduced curve, also returns a matrix T = [[a,b],[c,d]] and
    a number s, such that the reduced curve is f((ax+b)/(cx+d))*(cx+b)^6*s

    OUTPUT:
        
    A polynomial
    
    EXAMPLES:

    Over `\QQ`::

        sage: load("reduce.sage") # or use load("https://bitbucket.org/mstreng/reduce/raw/master/reduce_online.sage")
        sage: P.<x> = QQ[]
        sage: f = -16*x^6 - 1611*x^5 - 8640*x^4 + 69120*x^3 - 311040*x^2 + 746496*x - 746496
        sage: g = reduce_polynomial(f); g
        -x^6 + 3*x - 2
        sage: from sage.schemes.hyperelliptic_curves.invariants import igusa_clebsch_invariants
        sage: ic1 = igusa_clebsch_invariants(f)
        sage: ic2 = igusa_clebsch_invariants(g)
        sage: l = ic1[0]/ic2[0]
        sage: l
        136048896
        sage: l^2*ic2[1] == ic1[1]
        True
        sage: l^3*ic2[2] == ic1[2]
        True
        sage: l^5*ic2[3] == ic1[3]
        True

        sage: f =  -5*x^6 + 174*x^5 - 2700*x^4 + 21600*x^3 - 97200*x^2 + 233280*x - 233280
        sage: reduce_polynomial(f)
        -x^6 - x^5 - 5
        sage: g, T, s = reduce_polynomial(f,transformation = True)
        sage: g
        -x^6 - x^5 - 5
        sage: T
        [-6  0]
        [-1 -1]
        sage: s
        1/46656
        sage: f((T[0][0]*x+T[0][1])/(T[1][0]*x+T[1][1]))*(T[1][0]*x+T[1][1])^6*s == g
        True


    """
    if degree is None:
        degree = f.degree()
    if precision is None:
        precision = 3000
    k = f(0).parent()
#    f = reduce_gcd(f)

    if transformation:
        f, T1, sca = reduce_discriminant(f, degree=degree, factors=factors,  transformation=transformation)
        f, T2 = stoll_cremona_reduction(f, degree=degree, algorithm=algorithm,
                                precision=precision, transformation= transformation)
#    the following function is broken and needs to be fixed (TODO)
        try:
            g = reduce_height_with_unit(f)
        except NotImplementedError:
            pass
        return g, T1*T2, sca*(g/f)

    f = reduce_discriminant(f, degree=degree, factors=factors)
    f = stoll_cremona_reduction(f, degree=degree, algorithm=algorithm,
                                precision=precision)
#    the following function is broken and needs to be fixed (TODO)
    try:
        f = reduce_height_with_unit(f)
    except NotImplementedError:
        pass
    return f


def reduce_gcd(f):   
    r"""
    Takes out the common factors of the coefficients of f. In the case of
    number fields of class number larger than one, there may still be a small
    prime ideal in the denominator after the reduction.

    INPUT:
   
    - ``f`` - a polynomial
    
    OUTPUT:
 
    If the base ring is ZZ or QQ, then the output is an integer polynomial
    whose coefficients have gcd 1 and which is a rational scaling of f.
    
    If the base ring is a number field, then still remove many prime factors
    from the gcd of the coefficients (all in the case of class number one).
    
    EXAMPLES::

        sage: P.<x> = QQ[]
        sage: reduce_gcd(3*x^2+6*x+321)
        x^2 + 2*x + 107
        sage: reduce_gcd(321*x^6 + 1284*x^2 - 2247*x + 5136)
        x^6 + 4*x^2 - 7*x + 16
    
    NOTE::
    
        Currently not used anywhere.
    """
    k = f.base_ring()
    f = f.denominator()*f
    
    if k is QQ or k is ZZ:
        gcdofcoeffs = gcd([ZZ(coeff) for coeff in f.coefficients()])
        return f/gcdofcoeffs
    
    A = k.ideal(f.list())
    return f / gen_almost(A)


def gen_almost(A):
    r"""
    If A is a principal ideal, returns a generator.
    
    Otherwise, if A is a number field ideal, returns an element x of the number
    field such that x divides A and (A/x).norm() is small.
    
    This is an internal function, used by :func:`reduce_gcd`, so currently
    not used anywhere.
    
    EXAMPLES::
    
        sage: k = QuadraticField(-5,'a')
        sage: p = k.ideal(next_prime(2000)).factor()[1][0]; p
        Fractional ideal (2003, a + 1483)
        sage: x = gen_almost(p); x
        -27/2*a + 19/2
        sage: p/x
        Fractional ideal (2, a + 1)

    """
    if A.is_principal():
        return A.gens_reduced()[0]
    # Here is the obvious way:
    #B = A.reduce_equiv()
    #C = A/B
    #if not C.is_principal():
    #    raise RuntimeError
    
    # So we do something that looks stupid and slow,
    # but seemed to be faster:
    k = A.number_field()
    for c in k.class_group():
        B = c.ideal()
        C = A/B
        if C.is_principal():
            break

    if not B.is_integral():
        raise RuntimeError
    return C.gens_reduced()[0]


def power_of_ideal(A, e, factors=False):
    r"""
    Return A**e. If the class group is small, try to go via principal ideals.
    This was faster than the direct Sage function at the time of implementation.
    
    If factors is True, returns an element
    and an ideal, such that A**e is the product.
    """
    if A in QQ:
        if factors:
            return A**e, 1
        return A**e

    k = A.number_field()
    m = k.class_group()(A).order()
    q, r = ZZ(e).quo_rem(m)
    assert (A**m).is_principal()
    g = (A**m).gens_reduced()[0]
    x = g**q
    B = A**r
    if factors:
        return (x, B)
    return x*B


def reduce_discriminant(f, degree=None, factors=[], trial_division_bound=1000, verbose=False, triple_check=True, transformation = false):
    r"""
    Returns an equivalent binary form of the given degree. Tries to make
    the norm of the discriminant smaller. Uses the given auxiliary factors
    and some tricks to avoid factoring. Designed for class number one.
    
    INPUT:
        
    - ``f`` - a univariate polynomial of degree at most 6, interpreted as the
      binary form `F(X,Y) = Y^degree f(X/Y)`. The polynomial ``f`` is not
      allowed to have repeated roots and must have the given degree or one less.
    
    OUTPUT:

    a polynomial giving a `GL_2(QQ)` equivalent binary form
    
    EXAMPLES::

        sage: load("reduce.sage")
        sage: P.<x> = QQ[]
        sage: f = 32*x^6 + 6*x^2 - 5
        sage: factor(f.discriminant())
        2^33 * 3^6 * 5 * 13^2
        sage: g = reduce_discriminant(f); g
        x^6 + 3*x^2 - 10
        sage: factor(g.discriminant())
        2^13 * 3^6 * 5 * 13^2

    An example where the degree of the input is 5::

        sage: load("reduce.sage")
        sage: P.<y> = QQ[]
        sage: f = y^5 + 3^20*y + 3^20
        sage: g = f(y+1)
        sage: r = reduce_discriminant
        sage: f
        y^5 + 3486784401*y + 3486784401
        sage: r(f, degree=6)
        y^5 + 81*y + 1
        sage: g
        y^5 + 5*y^4 + 10*y^3 + 10*y^2 + 3486784406*y + 6973568803
        sage: r(g, degree=6)
        y^5 + 5*y^4 + 10*y^3 + 10*y^2 + 86*y + 83

    An example where a large power is removed::
    
        sage: load("reduce.sage")
        sage: x = QQ['x'].gen()
        sage: r = reduce_discriminant
        sage: f = ((x-2)*(x-2+3^20)*(x-2+3^10)*(x-2+3^20*7)*(x^2+x+1))
        sage: r(f, degree=6)
        3486784401*x^6 + 1647132543836838*x^5 + 85105305481926930091*x^4 + 85110864542496497113*x^3 + 7206360479055108*x^2 + 170852435649*x
        sage: r(f).discriminant().factor()
        -1 * 2^10 * 3^83 * 7^6 * 11^4 * 61^2 * 233^2 * 887^2 * 1549321^2 * 4607887^2 * 11920789^2 * 1964677987^2 * 3486489163^2 * 6188121169^2
        sage: f.discriminant().factor()
        -1 * 2^10 * 3^183 * 7^6 * 11^4 * 61^2 * 233^2 * 887^2 * 1549321^2 * 4607887^2 * 11920789^2 * 1964677987^2 * 3486489163^2 * 6188121169^2
        sage: r(f.reverse()).discriminant() == r(f).discriminant()
        True

    ALGORITHM:
        
            Algorithm 4.8 of Bouyer-Streng
    """
    K = f.base_ring()
    original_parent = f.parent()
    T = Matrix([[1,0],[0,1]])
    Sca = 1
    if K is QQ:
        K = NumberField(QQ['x'].gen(), 'a')
        f = f.base_extend(K)
    if degree is None:
        degree = f.degree()
    else:
        if not (degree == f.degree() or degree == f.degree()+1):
            raise ValueError
    if verbose:
        print("degree " + str(degree))
    from sage.schemes.hyperelliptic_curves.invariants import igusa_clebsch_invariants
    if degree == 6:
        aaa = sum([K.ideal(i) for i in igusa_clebsch_invariants(f)] +
                  [K.ideal(bin_form_discriminant(f, degree))])
    else:
        aaa = K.ideal(bin_form_discriminant(f, degree))
    if verbose:
        print("aaa " + str(aaa))
    A = [aaa]
    if verbose:
        print("starting trial division up to " + str(max(trial_division_bound, degree+1)))
    nrm = ZZ(aaa.norm())
    rational_primes = [p for p in prime_range(max(trial_division_bound, degree+1)) if p.divides(nrm)]
    for p in rational_primes:
        A = break_ideal_list(K.prime_factors(p), A)[1]
    if verbose:
        print("A = " + str(A))
        print("using coefficients and Igusa invariants")
    A = break_ideal_list(f.list(), A)[1]
    if degree == 6:
        A = break_ideal_list(list(igusa_clebsch_invariants(f)), A)[1]
    A = break_ideal_list(factors, A)[1]
    if verbose:
        print( "A = " + str(A))

    while True:
        if verbose:
            print( "Recognising powers and separating primes")
        A_primes, A_others = primes_and_powers(A)
        if verbose: print( "A_primes = " + str(A_primes))
        for p in A_primes:
            if verbose:
                print("Reducing at the prime " + str(p))
            if transformation:
                f, Tp, Scap = reduce_locally_without_modifications(f, p, degree, triple_check=(triple_check and K.degree()>1),transformation = True)
                T = T*Tp
                Sca = Sca*Scap
            else:
                f = reduce_locally_without_modifications(f, p, degree, triple_check=(triple_check and K.degree()>1))
        if verbose:
            print("A_others = " + str(A_others))
        if len(A_others) == 0:
            if transformation:
                        return original_parent(f.list()), T, Sca
            return original_parent(f.list())
        for a in A_others:
            if verbose: print( "a = " + str(a))
            if transformation:
                          g, Tp,Scap = reduce_locally_with_modifications(f, a, degree, transformation = True)
            else:
                          g = reduce_locally_with_modifications(f, a, degree)
            if verbose: print( "g = " + str(g))
            if g in f.parent():
                if g.discriminant().norm() < f.discriminant().norm():
                    if verbose: print( "norm became smaller " + str(g.discriminant().norm()) + " vs " + str(f.discriminant().norm()))
                    f = g
                    if transformation:
                          T = T*Tp
                          Sca = Sca*Scap
                    d = K.ideal(bin_form_discriminant(f, degree))
                    if degree == 6:
                        d = d +sum([K.ideal(i) for i in igusa_clebsch_invariants(f)])
                    A = [a+d for a in A_others]
                    A = [a for a in A if a != 1]
                    if verbose: print( "new A" + str(A))
                    break # exit the for loop
                else:
                    if verbose: print( "this a did not work")
                    continue # continue with the for loop
            else:
                if verbose: print( "found ideal factor " + str(g))
                A = break_ideal_list([g], A_others)[1]
                if verbose: print( "new A" + str(A))
                break # exit the for loop
        else: # loop was not broken: failed for every element of A_others
            # need to do more factoring
            print( "Starting factorization of all elements of " + str(A_others))
            A = []
            for a in A_others:
                print( "Factoring " + str(a))
                fct = factor(a)
                A = A + [p for (p,e) in fct]
            print( "Finished factoring. New A is " + str(A))


def reduce_locally_without_modifications(f, prime, degree=None, u_unif=None, l_unif=None, triple_check=True, transformation = False ):
    """
    This is Algorithm 4.5 of our article.
    
    INPUT:
        
    - ``f``, ``degree`` - ``f`` is a univariate polynomial of degree at most
    ``degree``, interpreted as the binary sextic form
    `F(X,Z) = Z^degree f(X/Z)`. The polynomial ``f`` is not
      allowed to have repeated roots.
    - ``ideal`` - a prime ideal
    - ``u_unif`` - a uniformizer such that u_unif / prime is integral
    - ``l_unif`` - a uniformizer such that prime / l_unif is integral

    OUTPUT:
        
    a polynomial giving a `GL_2` equivalent binary form of the given degree.
    
    NOTE::
    
        If prime is not principal, then the output may be non-optimal.
    """
    if degree is None:
        degree = f.degree()
    k = f.base_ring()
    if k is QQ:
        F = FiniteField(prime)
        if u_unif is None:
            u_unif = QQ(prime)
        if l_unif is None:
            l_unif = QQ(prime)
    else:
        F = prime.residue_field()
        if prime.is_principal():
            if u_unif is None:
                u_unif = prime.gens_reduced()[0]
            if l_unif is None:
                l_unif = prime.gens_reduced()[0]
        elif u_unif is None or l_unif is None:
            raise ValueError("Please supply u_unif and l_unif for non-principal ideal")

    x = f.parent().gen()
#    y = polygen(F)
    g = floor(degree/2) - 1
    f_input = f
    T = Matrix([[1,0],[0,1]])
    Sca = 1
    not_finished = True
    while not_finished:
        # Step 1 (as many times as we can)
        m = min([c.valuation(prime) for c in f.list()])
        f = f / l_unif**m
        Sca = Sca * (1 / l_unif**m)
        # Step 2
        for i in range(degree - g - 1, f.degree() + 1):
            if f[i].valuation(prime) + (degree-i) < g+2:
                break
        else:
            # F^dagger is integral
            f = f(x/l_unif) * u_unif**(degree - g - 2) # see equation (4.3)
            Sca = Sca * (u_unif**( - g - 2))
            T = T*Matrix([[1,0],[0,l_unif]])
            continue # continue with the next iteration (i.e., back to step 1)
        # Step 3
        #fbar = sum([F(f[i])*y**i for i in range(f.degree()+1)])
        rts = [root for (root, mult) in f.roots(F) if mult >= g+2]
        if len(rts) > 1:
            raise RuntimeError("This is not supposed to happen")
        if len(rts) == 0:
            # All three steps failed, stop the while loop.
            break
        root = rts[0]
        fdagger = f(u_unif*x+F.lift(root))/l_unif**(g+2) # eqn (4.3)
        for c in fdagger:
            if c.valuation(prime) < 0:
                # F^dagger is non-integral, so all three steps failed
                not_finished = False # stops the while loop at the end of its current iteration
                break # stop the for loop
            else:
                # F^dagger is integral
                f = fdagger
                Sca = Sca * (1/l_unif**(g+2))
                T = T*Matrix([[u_unif,F.lift(root)],[0,1]])
                break # stop the for loop
        
        
    if k == QQ and not f in ZZ['x']:
        raise RuntimeError("Bug in reduction of hyperelliptic curves: polynomial not integral")
    if triple_check and degree == 6:
        f_alternative = reduce_10th_power_prime(f_input, prime=prime, degree=degree, u_unif=u_unif, l_unif=l_unif)
        if f.discriminant().valuation(prime) != f_alternative.discriminant().valuation(prime):
            raise RuntimeError("Two local reduction algorithms (reduce_locally_without_modifications and reduce_10th_power_prime) disagree for " + str((f_input, prime, degree)))
        if prime + factorial(degree) == 1:
            f_alternative = reduce_locally_with_modifications(f, prime, degree=None, u_unif=None, l_unif=None)
            if f.discriminant().valuation(prime) != f_alternative.discriminant().valuation(prime):
                raise RuntimeError("Two local reduction algorithms (reduce_locally_without_modifications and reduce_locally_with_modifications) disagree for " + str((f_input, prime, degree)))
    if transformation:
        return f, T, Sca
    return f


def reduce_locally_with_modifications(f, ideal, degree=None, u_unif=None, l_unif=None, transformation = False):
    """
    This is Algorithm 4.5 of our article with the modifications of Lemma 4.6
    and Proposition 4.7.
    
    INPUT:
    
    - ``f``, ``degree`` - ``f`` is a univariate polynomial of degree at most
    ``degree``, interpreted as the binary sextic form
    `F(X,Z) = Z^degree f(X/Z)`. The polynomial ``f`` is not
      allowed to have repeated roots.
    - ``ideal`` - an ideal, must be coprime to factorial(degree)
    - ``u_unif`` - a uniformizer such that u_unif / ideal is integral
    - ``l_unif`` - a uniformizer such that ideal / l_unif is integral

    OUTPUT:
        
    Either a polynomial giving a `GL_2` equivalent binary form or a non-trivial
    factor of `ideal'.
    
    NOTE::
    
        If ideal is not squarefree, then the output may be non-optimal.
        If ideal is not principal, then the output may be non-optimal.
    """
    T = Matrix([[1,0],[0,1]])
    Sca = 1
    if ideal == 1:
        if transformation:
            return f, T, Sca
        return f
    if degree is None:
        degree = f.degree()
    k = f.base_ring()
    if k is QQ:
        if u_unif is None:
            u_unif = QQ(ideal)
        if l_unif is None:
            l_unif = QQ(ideal)
    else:
        if ideal.is_principal():
            if u_unif is None:
                u_unif = ideal.gens_reduced()[0]
            if l_unif is None:
                l_unif = ideal.gens_reduced()[0]
        elif u_unif is None or l_unif is None:
            raise ValueError("Please supply u_unif and l_unif for non-principal ideal")

    x = f.parent().gen()
#    y = polygen(F)
    g = floor(degree/2) - 1
    f_input = f
    not_finished = True
    while not_finished:
        # Step 1
        d, c = ideal_divides_or_factor(ideal, f.list())
        if d:
            f = f / l_unif
            Sca = Sca / l_unif
            continue # continue with the next iteration (i.e., step 1 again)
        if c != 1:
            return c # found a non-trivial factor
        # Step 2
        for i in range(degree - g - 1, f.degree() + 1):
            d, c = ideal_power_divides_or_factor(ideal, f[i], g+2-degree+i)
            if not d:
                if c != 1:
                    return c # found a non-trivial factor
                break # failed step 2, go to step 3
        else:
            # F^dagger is integral
            f = f(x/l_unif) * u_unif**(degree - g - 2) # see equation (4.3)
            Sca = Sca * (u_unif**( - g - 2))
            T = T*Matrix([[1,0],[0,l_unif]])
            continue # continue with the next iteration (i.e., back to step 1)
        # Step 3'
        derivs = []
        fder = f
        for i in range(g+2):
            derivs.append(fder)
            fder = fder.derivative()
        d, c = multi_poly_gcd_or_factor(derivs, ideal)
        if not d:
            return c # found a non-trivial factor
        c = poly_strip_zero_mod(c, ideal)
        lst = c.list()
        s = c.degree()
        if s == 0:
            # All three steps failed, stop the while loop.
            break
        if s in ideal:
            if factorial(degree) in ideal:
                raise ValueError("n! must be coprime to ideal, but n=%s and ideal=%s" % (degree, ideal))
            raise RuntimeError("s=%s, ideal=%s" % (s, ideal))
        t = -lst[s-1]*(s*lst[s]).inverse_mod(ideal)
        if not all([c in ideal for c in f(x+t).list()[:g+2]]):
            raise RuntimeError

        # fdagger = f(u_unif*x+t)/l_unif**(g+2) # eqn (4.3)
        fdagger_num = f(u_unif*x+t)
        for coeff in fdagger_num:
            d, c = ideal_power_divides_or_factor(ideal, coeff, g+2)
            if not d:
                if c != 1:
                    return c # found a non-trivial factor
                # F^dagger is non-integral, so all three steps failed
                not_finished = False # stops the while loop at the end of its current iteration
                break # stop the for loop
        else:
            # all coefficients were divisible
            f = fdagger_num / l_unif**(g+2)
            Sca = Sca * (1 / l_unif**(g+2))
            T = T*Matrix([[u_unif,t],[0,1]])            
            assert all([c.is_integral() for c in f.list()])
        
        
    if k == QQ and not f in ZZ['x']:
        raise RuntimeError("Bug in reduction of hyperelliptic curves: polynomial not integral")
    if transformation:
        return f, T, Sca
    return f


def reduce_10th_power_prime(f, prime, degree=None, u_unif=None, l_unif=None):
    r"""
    Removes 10th powers of the given prime from the discriminant of f.
    
    INPUT:
        
    - ``f`` - a univariate polynomial of degree at most 6, interpreted as the
      binary sextic form `F(X,Y) = Y^6 f(X/Y)`. The polynomial ``f`` is not
      allowed to have repeated roots.
    - ``prime`` - a prime number
    - ``u_unif`` - a uniformizer such that u_unif / prime is integral
    - ``l_unif`` - a uniformizer such that prime / l_unif is integral

    OUTPUT:
        
    a polynomial giving a `GL_2(QQ)` equivalent binary form with 10th powers
    removed from the discriminant where possible
    """
    if degree is None:
        degree = f.degree()
    if degree != 6:
        raise NotImplementedError("reduce_10th_power_prime is an old function, meant only for binary sextic forms, not for %s" % f)
    f_input = f
    k = f.base_ring()


    if k is QQ:
        F = FiniteField(prime)
        if u_unif is None:
            u_unif = QQ(prime)
        if l_unif is None:
            l_unif = QQ(prime)
    else:
        F = prime.residue_field()
        if prime.is_principal():
            if u_unif is None:
                u_unif = prime.gens_reduced()[0]
            if l_unif is None:
                l_unif = prime.gens_reduced()[0]
        elif u_unif is None or l_unif is None:
            raise ValueError("Please supply u_unif and l_unif for non-principal ideal")

    x = f.parent().gen()

    try_this_prime = True
    m = min([c.valuation(prime) for c in f.list()])
    f = f / l_unif**m
    while try_this_prime:
        try_this_prime = False
        maxpow = ZZ(min([(f[6].valuation(prime)-1)/3, \
                         (f[5].valuation(prime)-1)/2, \
                          f[4].valuation(prime)-1, \
                          f[3].valuation(prime)]).floor())
        if maxpow > 0:
            powers = max([[3*pow + \
                         min([f[indx].valuation(prime)-indx*pow \
                             for indx in range(7)]),pow] \
                         for pow in range(maxpow+1)])
            if powers[0] > 0:
                g = f(x/l_unif**powers[1]) * u_unif**(3*powers[1]-powers[0])
#                if not g.discriminant() == f.discriminant() / prime ** (10*powers[0]):
#                    raise RuntimeError("Bug in reduction of hyperelliptic curves: discriminant did not go down appropriately")
                if k == QQ and not g in ZZ['x']:
                    raise RuntimeError("Bug in reduction of hyperelliptic curves: polynomial not integral")
                try_this_prime = True
                f = g
        rts = f.roots(F)
        for ind in range(len(rts)):
            if rts[ind][1] >= 4:
                root_mod_prime = F.lift(rts[ind][0])
                g = f(x + root_mod_prime)
                maxpow = ZZ(min([(g[0].valuation(prime)-1)/3, (g[1].valuation(prime)-1)/2, g[2].valuation(prime)-1, g[3].valuation(prime)]).floor())
                if maxpow > 0:
                    powers = max([[-3*pow + min([g[indx].valuation(prime) + (indx)*pow for indx in range(7)]), pow] for pow in range(maxpow+1)])
                    if powers[0] > 0:
                        g = g(u_unif**powers[1]*x) / \
                            l_unif**(powers[0]+3*powers[1])
#                        if not g.discriminant() == f.discriminant() / prime ** (10*powers[0]):
#                            raise RuntimeError("Bug in reduction of hyperelliptic curves: discriminant did not go down appropriately")
                        if k == QQ and not g in ZZ['x']:
                            raise RuntimeError("Bug in reduction of hyperelliptic curves: polynomial not integral")
                        try_this_prime = True
                        f = g
    return f


def coprime_generator_representatives(k, A):
    r"""
    Given a number field k and an ideal A, returns a list of integral ideals,
    one for every generator of the class group, all of them coprime to A.
    
    This is an auxiliary function of :func:`coprime_representatives` and only
    used for number fields of class number >1.
    """
    c = k.class_group()
    gens = c.gens()
    gens_todo = [(gens[i], i) for i in range(len(gens))]
    primes = [None for g in gens]
    for p in k.primes_of_degree_one_iter():
        if p+A == 1:
            for a in gens_todo:
                if c(p) == a[0]:
                    primes[a[1]] = p
                    gens_todo.remove(a)
                    if len(gens_todo) == 0:
                        return primes
                    break


def coprime_representatives(k, A):
    r"""
    Given a number field k and an ideal A, returns a list of integral ideals,
    one for every ideal class, all of them coprime to A.
    
    This provides data used by :func:`almost_generator` and only
    used for number fields of class number >1.
    """
    c = k.class_group()
    l = coprime_generator_representatives(k, A)
    ret = []
    for a in c:
        m = a.list()
        ret.append(prod([l[i]**m[i] for i in range(len(m))]))
    return ret


def almost_generator(I, coprime_rep):
    r"""
    Given an ideal I and the output of :func:`coprime_representatives`, returns
    a generator of I times an element of ``coprime_rep``.
    
    This is only used for number fields of class number >1.
    """
    for J in coprime_rep:
        K = I*J
        if K.is_principal():
            return K.gens_reduced()[0]


def reduce_height_with_unit(f, precision=100):
    """
    Returns g = u*f for a unit u such that the maximum of all complex absolute
    values of all coefficients of g is minimal.
    
    INPUT:
        
    -``f``- a polynomial in a number field `K`
    
    OUTPUT:
    
    A polynomial of the form u*f, where u is a unit of the maximal order of `K`
    
    EXAMPLES::
    
        sage: X = var('X')
        sage: k = NumberField(X^2-41,'a')
        sage: a = k.an_element()
        sage: P.<x> = k[]
        sage: f = 2*x^4 + (64*a + 410)*x^3 + 1311360*a + 8396801
        sage: factor(f.discriminant())
        (-369836393348730718080*a - 2368108374136006451201) * (-118*a - 725) * (-118*a + 725) * (-1/2*a + 7/2)^4 * (1/2*a + 7/2)^4
        sage: g = reduce_height_with_unit(f); g
        (-640*a + 4098)*x^4 + (-64*a + 410)*x^3 + 320*a + 2049
        sage: factor(g.discriminant())
        (-1) * (-118*a - 725) * (-118*a + 725) * (-1/2*a + 7/2)^4 * (1/2*a + 7/2)^4


    Note that this does nothing over `\QQ`::
    
        sage: P.<x> = QQ[]
        sage: f = x^2 + 3
        sage: reduce_height_with_unit(f)
        x^2 + 3

    This is not implemented for number fields of degree greater than 2::

        sage: P.<y> = NumberField(x^3+2,'a')[]
        sage: reduce_height_with_unit(y^6+y+1)
        Traceback (most recent call last):
        ...
        NotImplementedError
    """
    k = f.base_ring()
    if k is QQ or k.degree() == 1 or (k.degree() == 2 and k.is_totally_imaginary()):
        return f
    if k.degree() > 2:
        raise NotImplementedError
    epsilon = k(k.unit_group().gens()[1])
    Phi = k.embeddings(RealField(precision))
    while  max([abs(phi(c)) for c in f.list() for phi in Phi]) >  max([abs(phi(c)) for c in (epsilon*f).list() for phi in Phi]):
        f = epsilon*f
    while  max([abs(phi(c)) for c in f.list() for phi in Phi]) >  max([abs(phi(c)) for c in ((epsilon^-1)*f).list() for phi in Phi]):
        f = (epsilon^-1)*f
    return f


def reduce_unit_in_disc(f, precision=100):
    """
    Reduce the height of the discriminant of f by multiplying f by a unit.
    
    INPUT:
        
    -``f``- a polynomial in a number field `K`
    
    OUTPUT:
    
    a polynomial of the form u*f, where u is a unit of the maximal order of `K`
    
    EXAMPLES::
    
        sage: X = var('X')
        sage: k = NumberField(X^2-41,'a')
        sage: a = k.an_element()
        sage: P.<x> = k[]
        sage: f = 2*x^4 + (64*a + 410)*x^3 + 1311360*a + 8396801
        sage: factor(f.discriminant())
        (-369836393348730718080*a - 2368108374136006451201) * (-118*a - 725) * (-118*a + 725) * (-1/2*a + 7/2)^4 * (1/2*a + 7/2)^4
        sage: g = reduce_unit_in_disc(f); g # not tested, broken
        (-640*a + 4098)*x^4 + (-64*a + 410)*x^3 + 320*a + 2049
        sage: factor(g.discriminant()) # not tested, above is broken
        (-1) * (-1/2*a - 7/2)^4 * (1/2*a - 7/2)^4 * (118*a + 725) * (118*a - 725)

    Note that this does nothing over `\QQ`::
    
        sage: P.<x> = QQ[]
        sage: f = x^2 + 3
        sage: reduce_unit_in_disc(f)
        x^2 + 3

    This is not implemented for number fields of degree greater than 2::

        sage: P.<y> = NumberField(x^3+2,'a')[]
        sage: reduce_unit_in_disc(y^6+y+1)
        Traceback (most recent call last):
        ...
        NotImplementedError
    """
    k = f.base_ring()
    if k is QQ or k.degree() == 1 or (k.degree() == 2 and k.is_totally_imaginary()):
        return f
    print( "WARNING: the function reduce_unit_in_disc will be removed, use reduce_height_with_unit instead.")
    if k.degree() > 2:
        raise NotImplementedError
    epsilon = k.unit_group().gens()[1]
    Phi = k.embeddings(RealField(precision))
    d = f.discriminant()
    e = abs(Phi[0](epsilon))**30
    while abs(Phi[0](d)/Phi[1](d)) < e:
        f = f/epsilon
        d = f.discriminant()
    e = e**-1
    while abs(Phi[0](d)/Phi[1](d)) > e:
        f = f/epsilon
        d = f.discriminant()
    return f


def bin_form_discriminant(f, degree):
    if f.base_ring().degree() == 1:
        f = QQ['x'](f.list())
    if f.degree() == degree:
        return f.discriminant()
    if f.degree() == degree - 1:
        return f.leading_coefficient()**2 * f.discriminant()
    raise ValueError


