def ideal_nth_root(I, n):
    """
    Given a number field ideal I and a positive integer n, returns the n-th
    root of the ideal I. Actually, if the n-th root is principal, this function
    may return a generator. This function may be slow in class number > 1.
    """
    fact = I.factor()
    ret = 1
    for (p, e) in fact:
        if e % n != 0:
            raise ValueError("Ideal %s is not an nth power for n=%s, because prime %s appears to power %s" % (I, n, p, e))
        if p.is_principal():
            ret = ret * p.gens_reduced()[0]**ZZ(e/n)
        else:
            ret = ret * p**ZZ(e/n)
    return ret
    
def check_curve(p, phi, f, print_charpol=False):
    """
    NOTE: Uses Magma
    
    Input: a morphism phi from a real quadratic field F to a quartic CM-field
    E. A prime number p, and a polynomial f over F of degree 5 or 6.
    It assumes p is totally split in the codomain of phi.
    
    It is False if the input is inconsistent with y^2=f having CM with reflex
    field E.
    Otherwise, the output is True.
    
    EXAMPLE::
    
        sage: load("reduce.sage") # load("https://bitbucket.org/mstreng/reduce/raw/master/reduce_online.sage")
        sage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage")
        sage: data = cm_reflex_field("2b")[3]
        sage: data
        Hyperelliptic Curve over Number Field in a with defining polynomial t^2 + t - 15 defined by y^2 = (-a + 3)*x^6 + (8*a - 34)*x^5 + (-37*a + 80)*x^4 + (20*a - 220)*x^3 + (-56*a - 65)*x^2 + (-22*a - 136)*x - 9*a - 37, which has CM by the maximal order of Number Field in r with defining polynomial t^4 + 17*t^2 + 61
        sage: f = data.f()
        sage: E = CM_Field(data.reflex_DAB())
        sage: F = f.base_ring()
        sage: [phi1, phi2] = F.embeddings(E)
        sage: check_curve(19, phi1, f)
        False
        sage: check_curve(19, phi2, f)
        True
        sage: check_curve(149, phi1, f)
        False
        sage: check_curve(149, phi2, f)
        True
        sage: check_curve(179, phi1, f)
        False
        sage: check_curve(179, phi2, f)
        True
        sage: check_curve(271, phi1, f)
        False
        sage: check_curve(271, phi2, f)
        True

    """
    E = phi.codomain()
    fact = E.ideal(p).factor()
    P = fact[0][0]
    r = P.residue_field()
    k = FiniteField(p)
    Q = k['x']
    Phi = E.CM_types()[0]
    K = Phi.reflex_field()
    X = K['X'].gen()
    if is_Polynomial(f):
        H = magma(HyperellipticCurve(Q([k(r(phi(c))) for c in list(f)])))
    else:
        H = magma([k(r(phi(c))) for c in f]).HyperellipticCurveFromIgusaClebsch()
    frob_pol = (H.Jacobian().EulerFactor().sage()(1/X)*X^4).numerator()
    if print_charpol:
        print(frob_pol)
    for r in frob_pol.roots():
        if K.ideal(r[0]) == Phi.type_norm(P):
            return True
    return False


def cm_reflex_field(case):
    
    """
    A code to load the data contained in the various table described in [B-S]
    
    INPUT:
    
    - ``case`` - the cases as described in [B-S]. This has to be one of the following
       "1a", "1b", "1c", "2a", "2b", "2c".
       
    OUTPUT:
        
    A list of SageObject of class CMCurveData. Each entry correspond to an entry
    in the corresponding table.
    
    EXAMPLES:
    
    An example showing how to load the data::
        
        sage: load("reduce.sage")
        sage: ListOfData = cm_reflex_field("2b")
        sage: ListOfData[3]
        Hyperelliptic Curve over Number Field in a with defining polynomial
        t^2 + t - 15 defined by y^2 = (-a + 3)*x^6 + (8*a - 34)*x^5 +
        (-37*a + 80)*x^4 + (20*a - 220)*x^3 + (-56*a - 65)*x^2 + (-22*a - 136)*x
        - 9*a - 37, which has CM by the maximal order of Number Field in r with
        defining polynomial t^4 + 17*t^2 + 61

    The same example, but loading the data remotely::
    
        sage: load("https://bitbucket.org/mstreng/reduce/raw/master/reduce_online.sage")
        sage: ListOfData = cm_reflex_field("2b")
        sage: ListOfData[3]
        Hyperelliptic Curve over Number Field in a with defining polynomial
        t^2 + t - 15 defined by y^2 = (-a + 3)*x^6 + (8*a - 34)*x^5 +
        (-37*a + 80)*x^4 + (20*a - 220)*x^3 + (-56*a - 65)*x^2 + (-22*a - 136)*x
        - 9*a - 37, which has CM by the maximal order of Number Field in r with
        defining polynomial t^4 + 17*t^2 + 61
 
    Each entry has more data associated to it, as described in [B-S]::
    
        sage: load("reduce.sage")
        sage: Curve = cm_reflex_field("2b")[3]
        sage: Curve.DAB()
        [5, 17, 61]
        sage: Curve.Igusa_invariants()
        [2464*a + 41440, 131072*a + 389120, -38076416*a + 5512560640]
        sage: Curve.discriminant()
        -181743169402044416*a + 618858180263280640
        sage: Curve.stable_discriminant()
        3481157680*a + 15334934161
        sage: Curve.curve()
        Hyperelliptic Curve over Number Field in a with defining polynomial t^2 + t - 15
        defined by y^2 = (-a + 3)*x^6 + (8*a - 34)*x^5 + (-37*a + 80)*x^4 +
        (20*a - 220)*x^3 + (-56*a - 65)*x^2 + (-22*a - 136)*x - 9*a - 37
        sage: Curve.f()
        (-a + 3)*x^6 + (8*a - 34)*x^5 + (-37*a + 80)*x^4 + (20*a - 220)*x^3
        + (-56*a - 65)*x^2 + (-22*a - 136)*x - 9*a - 37

    Note that some of the properties are linked to specific cases::
    
        sage: load("reduce.sage")
        sage: Curve = cm_reflex_field("2b")[3]
        sage: Curve.obstructions()
        []
        sage: Curve = cm_reflex_field("2c")[3]
        sage: Curve.obstructions()
        [Fractional ideal (-3*a + 16), Fractional ideal (-3*a - 19)]
        sage: Curve.discriminant()
        Traceback (most recent call last):
        ...
        NotImplementedError: This curve is not defined over a quadratic field, hence we have not computed a model of it.


    REFERENCES
    
    [B-S]   F.Bouyer & M.Streng "Examples of CM curves of genus two defined over the reflex field"
            arXiv:1307.0486
    """

    ListOfCurves = []
    Q.<t> = QQ[]
    
    if not (len(case) == 2 and case[0] in ["1", "2"] and case[1] in ["a", "b", "c"]):
        raise ValueError("Invalid case:" + str(case))
    
    if case == "1c" or case == "2a":
        return []
    
    import urllib.request
    ur = cm_curve_table_base_url + case
    with urllib.request.urlopen(ur) as data: # data = open(ur, "r")
        lines = data.readlines() 
    #parse data into curves
    if case == "1a":
        P.<x> = QQ[]
        for line in lines:
            DAB = eval(line)[0]
            K.<r> = NumberField(t^4+ZZ(DAB[1])*t^2+ZZ(DAB[2]))
            Coeffs = eval(line)[-1]
            f = P([Coeffs[i][0] for i in srange(len(Coeffs))])
            inv = eval(line)[1]
            ListOfCurves += [CMCurveData(QQ, f, K, K, inv, [], DAB, DAB)]
    if case == "1b" or case == "2b":
        for line in lines:
            DAB = eval(line)[0]
            K.<r> = NumberField(t^4+ZZ(DAB[1])*t^2+ZZ(DAB[2]))
            try:
                DABr = DAB_to_minimal(CM_Type_quartic(DAB).reflex()._DAB)
                Kr.<alpha> = NumberField(t^4+ZZ(DABr[1])*t^2+ZZ(DABr[2]))
            except(NameError, AttributeError):
                Kr = None
                DABr = None
            L.<a> = NumberField(Q(eval(line)[1]))
            P.<x> = L[]
            Coeffs = eval(line)[-1]
            f = P([Coeffs[i][0]+a*Coeffs[i][1] for i in srange(len(Coeffs))])
            inv = [eval(line)[2][i][0]+a*eval(line)[2][i][1] for i in srange(len(eval(line)[2]))]
            ListOfCurves += [CMCurveData(L, f, K, Kr, inv, [], DAB, DABr)]
    if case == "2c":
        for line in lines:
            DAB = eval(line)[0]
            K.<r> = NumberField(t^4+ZZ(DAB[1])*t^2+ZZ(DAB[2]))
            try:
                DABr = DAB_to_minimal(CM_Type_quartic(DAB).reflex()._DAB)
                Kr.<alpha> = NumberField(t^4+ZZ(DABr[1])*t^2+ZZ(DABr[2]))
            except(NameError, AttributeError):
                Kr = None
                DABr = None
            L.<a> = NumberField(Q(eval(line)[1]))
            P.<x> = L[]
            inv = [eval(line)[2][i][0]+a*eval(line)[2][i][1] for i in srange(len(eval(line)[2]))]
            Loc = eval(line)[-1]
            obs = [L.ideal([Loc[i][0][j][0]+a*Loc[i][0][j][1] for j in srange(2)]) for i in srange(len(Loc))]
            ListOfCurves += [CMCurveData(L, None, K, Kr, inv, obs, DAB, DABr)]
    return ListOfCurves
    
    
class CMCurveData(SageObject):
    def __init__(self, base_field, f, CM_field, reflex_field, Igusa_invariants, obstructions, DAB, reflex_DAB):
        self._base_field = base_field
        self._f = f
        if f is None:
            self._curve = None
        else:
            self._curve = HyperellipticCurve(f)
        self._CM_field = CM_field
        self._reflex_field = reflex_field
        self._Igusa_invariants = Igusa_invariants
        self._obstructions = obstructions
        self._DAB = DAB
        self._reflex_DAB = reflex_DAB

    def f(self):
        return self._f
    
    def base_field(self):
        return self._base_field

    def curve(self):
        if self._curve is None:
            raise NotImplementedError("This curve is not defined over a quadratic field, hence we have not computed a model of it.")
        return self._curve

    def CM_field(self):
        return self._CM_field
        
    def reflex_field(self):
        if self._reflex_field is None:
            raise NotImplementedError('Please load the "recip" package in order to be able to compute the reflex field. Then read the CM curve data anew. See https://bitbucket.org/mstreng/recip\nTo load the package directly into Sage, type\nsage: load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage")')
        return self._reflex_field
        
    def base_field_to_reflex_field(self):
        if self._reflex_field is None:
            return self.reflex_field()
        F = self.f().base_ring()
        d = self.f().discriminant().norm()
        E = CM_Field(self.reflex_DAB())
        ps = []
        for p in Primes():
            if p > 30 and not p.divides(d) and len(E.ideal(p).factor())==4:
                ps.append(p)
                if len(ps) == 10:
                    break
        phis = F.embeddings(E)
        answers = [[check_curve(p, phi, self.f()) for p in ps] for phi in phis]
        if answers[1][0]:
            answers = [answers[1], answers[0]]
            phi = phis[1]
        else:
            phi = phis[0]
        if not self.CM_field().is_isomorphic(self.reflex_field()):
            if (not all(answers[0])) or not all([not a for a in answers[1]]):
                print("% TODO: The following curve is incorrect,")
                print("%       replace it by the correct curve!")
                print("%       Answers to check_curve were:")
                print("% " + str(answers[0]) + str(answers[1]))
                print("%       For the primes" + str(ps))
                print("% The correct endomorphism ring can be derived")
                print("% heuristically from the following characteristic")
                print("% polynomials of Frobenius (assuming the minimal polynomial of a is correct):")
                for p in ps:
                    check_curve(p, phis[0], f, print_charpol=True)
            return phi.im_gens()[0]
        else:
            if not (all(answers[0]) and all(answers[1])):
                print("% TODO: The following curve is incorrect,")
                print("%       replace it by the correct curve!")
                print("%       Answers to check_curve were:")
                print("% " + str(answers[0]) + str(answers[1]))
                print("%       For the primes" + str(ps))
                print("% The correct endomorphism ring can be derived")
                print("% heuristically from the following characteristic")
                print("% polynomials of Frobenius (assuming the minimal polynomial of a is correct):")
                for p in ps:
                    check_curve(p, phis[0], f, print_charpol=True)
            return "The reflex field and the CM field are isomorphic"

    def reflex_f(self):
        if self._reflex_field is None:
            return self.reflex_field()
        if self.CM_field().is_isomorphic(self.reflex_field()):
            return self.f()
        a_alt = self.base_field_to_reflex_field()
        if a_alt[2] < 0:
            sigma = self.base_field().galois_group().gen()
            return self.f().parent()([sigma(c) for c in self.f().list()])
        return self.f()
    
    def Igusa_invariants(self):
        return self._Igusa_invariants
    
    def discriminant(self):
        if self._f is None:
            return self.curve()
        if self.f().degree() == 6:
            return self.f().discriminant()*(2^8)
        if self.f().degree() == 5:
            return self.f().discriminant()*(2^8)*(self.f()[5]^2)
            
    def stable_discriminant(self):
        if self._f is None:
            return self.curve()
        from sage.schemes.hyperelliptic_curves.invariants import igusa_clebsch_invariants
        I2,I4,I6,I10 = igusa_clebsch_invariants(self.f())
        J2 = I2/(2^3)
        J10 = I10/(2^12)
        J4 = (4*J2^2 - I4)/(3*2^5)
        J6 = (8*J2^3 - 160*J2*J4 - I6)/(2^6*3^2)
        J8 = (J2*J6 - J4^2)/(2^2)
        absolute_algebraic_invariants = [J2^5/J10,J2^3*J4/J10,J2^2*J6/J10,J2*J8/J10,J4*J6/J10,J4*J8^2/J10^2,J6^2*J8/J10^2,J6^5/J10^3,J6*(J8^3)/J10^3,J8^5/J10^4]
        D_power_minus12 = 0
        for indx in srange(len(absolute_algebraic_invariants)):
            if indx < 5:
                root = 1
            elif indx < 7:
                root = 2
            elif indx < 9:
                root = 3
            else:
                root = 4
            denominator_this_invariant_power_12 = absolute_algebraic_invariants[indx].denominator_ideal()**ZZ(12/root)
            D_power_minus12 = D_power_minus12 + 1/denominator_this_invariant_power_12
        try:
            return ideal_nth_root(D_power_minus12, -12)
        except ValueError:
            raise NotImplementedError("The minimal discriminant over the algebraic closure is not defined over the base field for the curve y^2 = %s" % self.f())

   
    def obstructions(self):
        return self._obstructions
    
    def DAB(self):
        return self._DAB
    
    def reflex_DAB(self):
        if self._reflex_DAB is None:
            return self.reflex_field()
        return self._reflex_DAB

    def __repr__(self):
        """
        Returns a string representation of these CM curve data.
        """
        if self._curve is None:
            return "Curve with CM by the maximal order of K = %s, defined over K^r, but not over K_0^r." % self.CM_field()
        return "%s, which has CM by the maximal order of %s" % (self.curve(), self.CM_field())

